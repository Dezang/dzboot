package net.dezang.dzboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DzbootApplication {

    public static void main(String[] args) {
        SpringApplication.run(DzbootApplication.class, args);
    }
}
